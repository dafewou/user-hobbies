import express from "express"
import bodyParser from "body-parser"
import UsersHandler from "../handlers/UsersHandler"
import UserHandler from "../handlers/UserHandler"
import UserHobbiesHandler from "../handlers/UserHobbiesHandler"
import HobbyHandler from "../handlers/HobbyHandler"

const router = express.Router()

// Middleware
router.use(bodyParser.json())

/*
 * Routes
 */
// Users
router.route("/users")
  .get(UsersHandler.onGet)
  .post(UsersHandler.onPost)

router.route("/users/:id")
  .get(UserHandler.onGet)
  .post(UserHandler.onPost)
  .delete(UserHandler.onDelete)

// User Hobbies
router.route("/users/:userID/hobbies")
  .get(UserHobbiesHandler.onGet)
  .post(UserHobbiesHandler.onPost)

router.route("/hobbies/:id")
  .get(HobbyHandler.onGet)
  .post(HobbyHandler.onPost)
  .delete(HobbyHandler.onDelete)

export default router