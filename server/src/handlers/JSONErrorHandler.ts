import { Request, Response, NextFunction } from "express"

export const onError = async (err: Error, _req: Request, res: Response, _next: NextFunction) => {
  // Respond and log
  const respondAndLog = (statusCode: number, responseObject: object) => {
    if (statusCode >= 500) {
      console.error(err.stack)
    }

    // Always return JSON
    return res.status(statusCode).json(responseObject)
  }

  // Check error types
  switch (err.name) {
    case "UnauthorizedError": {
      respondAndLog(401, {
        type:    "unauthorized_access",
        message: err.message,
      })
      break
    }

    case "ResourceNotFoundError": {
      respondAndLog(404, {
        type:    "resource_not_found",
        message: err.message,
      })
      break
    }

    case "ValidationError": {
      respondAndLog(422, {
        type:    "validation_error",
        message: err.message,
      })
      break
    }

    case "DatabaseError": {
      respondAndLog(500, {
        type:    "database_error",
        message: err.message,
      })
      break
    }

    default: {
      console.warn(`Did not recognize ${err.constructor.name}.`)
      respondAndLog(500, {
        type:    "unknown_error",
        message: err.message,
      })
      break
    }
  }
}