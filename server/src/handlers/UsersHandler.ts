import { Request, Response, NextFunction } from "express"
import Joi from "@hapi/joi"
import { ValidationError } from "../errors"
import UserStore from "../stores/UserStore"

// Define schema for validation
const validationSchema = Joi.object().keys({
  name: Joi.string().required(),
})

const onGet = async (req: Request, res: Response, next: NextFunction) => {
  const { page = 1, maxResults = 30 } = req.query

  try {
    // Fetch users and paginationInfo
    const [users, paginationInfo] = await Promise.all([
      UserStore.fetchAll(parseInt(page), parseInt(maxResults)),
      UserStore.fetchPaginationInfo(parseInt(page), parseInt(maxResults)),
    ])

    // Response
    return res.json({
      users,
      paginationInfo,
    })
  } catch (error) {
    next(error)
    return
  }
}

const onPost = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { value: { name }, error } = validationSchema.validate(req.body)
    if (error != null) {
      next(new ValidationError(`${error}`))
      return
    }

    // Create user
    const user = await UserStore.create(name)

    // Response
    return res.json({
      user,
    })
  } catch (error) {
    next(error)
    return
  }
}

const UsersHandler = {
  onGet,
  onPost,
}
export default UsersHandler