import { Request, Response, NextFunction } from "express"
import Joi from "@hapi/joi"
import { ResourceNotFoundError, ValidationError } from "../errors"
import UserStore from "../stores/UserStore"
import HobbyStore from "../stores/HobbyStore"
import { passionLevel } from "../model/Hobby"

// Define schema for validation
const validationSchema = Joi.object().keys({
  passionLevel: Joi.string().required().valid(passionLevel.Low, passionLevel.Medium, passionLevel.High, passionLevel.VeryHigh),
  name:         Joi.string().required(),
  year:         Joi.string().required(),
})

const onGet = async (req: Request, res: Response, next: NextFunction) => {
  const { userID } = req.params

  try {
    // Fetch users hobbies
    const hobbies = await HobbyStore.fetchAllForUser(userID)

    // Response
    return res.json({
      hobbies,
    })
  } catch (error) {
    next(error)
    return
  }
}

const onPost = async (req: Request, res: Response, next: NextFunction) => {
  const { userID } = req.params

  try {
    const { value: { passionLevel, name, year }, error } = validationSchema.validate(req.body)
    if (error != null) {
      next(new ValidationError(`${error}`))
      return
    }

    // Fetch user by ID
    const user = await UserStore.fetchByID(userID)
    if (user == null) {
      next(new ResourceNotFoundError("User doesn't exist"))
      return
    }

    // Create hobby
    const hobby = await HobbyStore.create(passionLevel, name, year, user.id)

    // Response
    return res.json({
      hobby,
    })
  } catch (error) {
    next(error)
    return
  }
}

const UserHobbiesHandler = {
  onGet,
  onPost,
}
export default UserHobbiesHandler
