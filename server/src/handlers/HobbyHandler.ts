import { Request, Response, NextFunction } from "express"
import Joi from "@hapi/joi"
import { ResourceNotFoundError, ValidationError } from "../errors"
import HobbyStore from "../stores/HobbyStore"
import { passionLevel } from "../model/Hobby"

// Define schema for validation
const validationSchema = Joi.object().keys({
  passionLevel: Joi.string().required().valid(passionLevel.Low, passionLevel.Medium, passionLevel.High, passionLevel.VeryHigh),
  name:         Joi.string().required(),
  year:         Joi.string().required(),
})

const onGet = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params

  try {
    // Fetch hobby by ID
    const hobby = await HobbyStore.fetchByID(id)
    if (hobby == null) {
      next(new ResourceNotFoundError("Hobby not found"))
      return
    }

    // Response
    return res.json({
      hobby,
    })
  } catch (error) {
    next(error)
    return
  }
}

const onDelete = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params

  try {
    // Fetch hobby by ID
    const hobby = await HobbyStore.fetchByID(id)
    if (hobby == null) {
      next(new ResourceNotFoundError("Hobby not found"))
      return
    }

    // delete hobby
    const deletedHobby = await HobbyStore.delete(id)

    // Response
    return res.json({
      hobby: deletedHobby,
    })
  } catch (error) {
    next(error)
    return
  }
}

const onPost = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params

  try {
    const { value: { passionLevel, name, year }, error } = validationSchema.validate(req.body)
    if (error != null) {
      next(new ValidationError(`${error}`))
      return
    }

    // Fetch hobby by ID
    const hobby = await HobbyStore.fetchByID(id)
    if (hobby == null) {
      next(new ResourceNotFoundError("Hobby not found"))
      return
    }

    const updatedHobby = await HobbyStore.update(id, passionLevel, name, year)
    // Response
    return res.json({
      user: updatedHobby,
    })
  } catch (error) {
    next(error)
    return
  }
}

const HobbyHandler = {
  onGet,
  onDelete,
  onPost,
}
export default HobbyHandler