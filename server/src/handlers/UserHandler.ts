import { Request, Response, NextFunction } from "express"
import Joi from "@hapi/joi"
import { ResourceNotFoundError, ValidationError } from "../errors"
import UserStore from "../stores/UserStore"

// Define schema for validation
const validationSchema = Joi.object().keys({
  name: Joi.string().required(),
})

const onGet = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params

  try {
    // Fetch user by ID
    const user = await UserStore.fetchByID(id)
    if (user == null) {
      next(new ResourceNotFoundError("User not found"))
      return
    }

    // Response
    return res.json({
      user,
    })
  } catch (error) {
    next(error)
    return
  }
}

const onDelete = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params

  try {
    // Fetch user by ID
    const user = await UserStore.fetchByID(id)
    if (user == null) {
      next(new ResourceNotFoundError("User not found"))
      return
    }

    // delete user
    const deletedUser = await UserStore.delete(id)

    // Response
    return res.json({
      user: deletedUser,
    })
  } catch (error) {
    next(error)
    return
  }
}

const onPost = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params

  try {
    const { value: { name }, error } = validationSchema.validate(req.body)
    if (error != null) {
      next(new ValidationError(`${error}`))
      return
    }

    // Fetch user by ID
    const user = await UserStore.fetchByID(id)
    if (user == null) {
      next(new ResourceNotFoundError("User not found"))
      return
    }

    // Update user
    const updatedUser = await UserStore.update(id, name)

    // Response
    return res.json({
      user: updatedUser,
    })
  } catch (error) {
    next(error)
    return
  }
}

const UserHandler = {
  onGet,
  onDelete,
  onPost,
}
export default UserHandler