import User, { UserObject, IUser } from "../model/User"
import PaginationInfo from "../model/PaginationInfo"
import { DatabaseError } from "../errors"

export default class UserStore {

  /**
   * Create a new user document
   * @static
   * @param {string} name - The user name
   * @return {Promise<UserObject>}
   */
  static create = async (name: string): Promise<UserObject> => {
    try {
      // create new user model
      const user = new User({ name })
      // save and return user
      await user.save()
      return new UserObject(user)
    } catch (err) {
      throw new DatabaseError("Failed to create user", err)
    }
  }

  /**
   * Update user document
   * @static
   * @param {string} id - The user id
   * @param {string} name - The user name
   * @return {Promise<UserObject>}
   */
  static update = async (id: string, name: string): Promise<UserObject> => {
    try {
      // find and update user document
      await User.findByIdAndUpdate(id, { $set: { name } })

      const updatedUser = await User.findById(id)
      // return updated user
      return new UserObject(updatedUser as IUser)
    } catch (err) {
      throw new DatabaseError("Failed to update user", err)
    }
  }

  /**
   * delete user document
   * @static
   * @param {string} id - The user id
   * @return {Promise<UserObject>}
   */
  static delete = async (id: string): Promise<UserObject> => {
    try {
      // Fetch user
      const user = await User.findById(id) as IUser

      // delete user document
      await user.remove()

      // return deleted user
      return new UserObject(user)
    } catch (err) {
      throw new DatabaseError("Failed to delete user", err)
    }
  }

  /**
   * Fetch user by ID
   * @static
   * @param {string} id - The user id
   * @return {Promise<UserObject>}
   */
  static fetchByID = async (id: string): Promise<UserObject | null> => {
    try {
      // Fetch user
      const user = await User.findById(id)

      // Return user
      return new UserObject(user as IUser)
    } catch (err) {
      // Return null on no match
      return null
    }
  }

  /**
   * Fetch All users
   * @param page
   * @param maxResults
   * @returns {Promise<UserObject[]>}
   */
  static fetchAll = async (page: number = 1, maxResults: number = 30): Promise<UserObject[]> => {
    try {
      // fetch all users
      const users = await User.find({}, null, { skip: (maxResults * (page - 1)), limit: maxResults })

      // return list of users
      return users.map(user => new UserObject(user))
    } catch (err) {
      throw new DatabaseError("Failed to fetch all user", err)
    }
  }

  /**
   * Fetch user pagination
   * @param page
   * @param maxResults
   * @returns {Promise<PaginationInfo>}
   */
  static fetchPaginationInfo = async (page: number = 1, maxResults: number = 30): Promise<PaginationInfo> => {
    try {
      // fetch all users
      const totalUsers = await User.countDocuments()

      // return list of users
      return new PaginationInfo(totalUsers, page, maxResults)
    } catch (err) {
      throw new DatabaseError("Failed to fetch user pagination", err)
    }
  }
}
