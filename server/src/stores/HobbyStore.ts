import Hobby, { HobbyObject, IHobby } from "../model/Hobby"
import { DatabaseError } from "../errors"

export default class HobbyStore {

  /**
   * Create a new hobby document
   * @static
   * @param {string} passionLevel - The hobby passionLevel
   * @param {string} name - The hobby name
   * @param {string} year - The hobby since year
   * @param {string} userID - The user adding the hobby
   * @return {Promise<HobbyObject>}
   */
  static create = async (passionLevel: string, name: string, year: string, userID: string): Promise<HobbyObject> => {
    try {
      // create new hobby model
      const hobby = new Hobby({ passionLevel, name, year, user: userID })

      // save and return user hobby
      await hobby.save()
      return new HobbyObject(hobby)
    } catch (err) {
      throw new DatabaseError("Failed to create hobby for user", err)
    }
  }

  /**
   * Update hobby document
   * @static
   * @param {string} id - The hobby id
   * @param {string} passionLevel - The hobby passionLevel
   * @param {string} name - The hobby name
   * @param {string} year - The hobby since year
   * @return {Promise<HobbyObject>}
   */
  static update = async (id: string, passionLevel: string, name: string, year: string): Promise<HobbyObject> => {
    try {
      // find and update hobby document
      await Hobby.findByIdAndUpdate(id, { $set: { passionLevel, name, year } })

      const updatedHobby = await Hobby.findById(id)
      // return updated hobby
      return new HobbyObject(updatedHobby as IHobby)
    } catch (err) {
      throw new DatabaseError("Failed to update hobby", err)
    }
  }

  /**
   * delete hobby document
   * @static
   * @param {string} id - The hobby id
   * @return {Promise<HobbyObject>}
   */
  static delete = async (id: string): Promise<HobbyObject> => {
    try {
      // find and delete hobby
      const deletedHobby = await Hobby.findByIdAndDelete(id)

      // return deleted hobby
      return new HobbyObject(deletedHobby as IHobby)
    } catch (err) {
      throw new DatabaseError("Failed to delete hobby", err)
    }
  }

  /**
   * Fetch hobby by ID
   * @static
   * @param {string} id - The hobby id
   * @return {Promise<HobbyObject>}
   */
  static fetchByID = async (id: string): Promise<HobbyObject | null> => {
    try {
      // Fetch hobby
      const hobby = await Hobby.findById(id)

      // Return hobby
      return new HobbyObject(hobby as IHobby)
    } catch (err) {
      // Return null on no match
      return null
    }
  }

  /**
   * Fetch All hobbies for user
   * @param {string} userID - The user id to fetch for
   * @returns {Promise<HobbyObject[]>}
   */
  static fetchAllForUser = async (userID: string): Promise<HobbyObject[]> => {
    try {
      // fetch all hobbies
      const hobbies = await Hobby.find({ user: userID })

      // return list of user hobbies
      return hobbies.map(hobby => new HobbyObject(hobby))
    } catch (err) {
      throw new DatabaseError("Failed to fetch all hobbies for user", err)
    }
  }
}
