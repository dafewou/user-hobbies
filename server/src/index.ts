import express from "express"
import cors from "cors"
import v1 from "./routes/v1"
import { onError } from "./handlers/JSONErrorHandler"
import Config from "./helpers/Config"
import DB from "./model/DB"
import swaggerUi from "swagger-ui-express"
import * as swaggerDocument from "./swagger.json"

const PORT = Config.shared.requireProduction("PORT", "8080")
const CORS_HOST = Config.shared.requireProduction(
  "CORS_HOST",
  "http://localhost:9090",
)

// Verify config
Config.shared.verify()

// Check connection to mongo db
DB.shared()
  .then(() => console.log("Connection to DB successful"))
  .catch(err => {
    console.log(
      "Unable to connect to mongo DB, check if mongod service started.",
      err,
    )
    process.exit(0)
  })

const app = express()

// Set up CORS
const corsOptions: cors.CorsOptions = {
  allowedHeaders:    ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
  credentials:       true,
  methods:           "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  origin:            CORS_HOST,
  preflightContinue: false,
}
app.use(cors(corsOptions))
app.options("*", cors(corsOptions))

// Set up swagger ui
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument))

// Set up router
app.use("/v1", v1)

// Handle error
app.use(onError)

// Listen and serve
app.listen(PORT, () => console.log(`Server listening on port ${PORT}`))