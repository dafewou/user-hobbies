import UserStore from "../../stores/UserStore"
import users from "../fixtures/Users"
import paginationInfo from "../fixtures/PaginationInfo"
import chai, { expect, assert, should } from "chai"
import chaiHttp from "chai-http"
import Server from "../test-server"
import sinon from "sinon"
import * as http from "http"
import { IUser, UserObject } from "../../model/User"

should()
chai.use(chaiHttp)

describe("Users", () => {
  let server: http.Server
  const sandbox = sinon.createSandbox()

  before("All tests, start the server", async () => {
    server = await Server.start()
  })

  describe("/GET users", () => {
    before("All tests, mock user response", async () => {
      sandbox.stub(UserStore, "fetchAll").callsFake(async () => users)
      sandbox.stub(UserStore, "fetchPaginationInfo").callsFake(async () => paginationInfo)
    })

    it("should return status 200 along with users data", async () => {
      const request = await chai.request(server)
      const res = await request.get("/v1/users")
      res.status.should.eql(200)
      res.body.users.length.should.eql(3)
      expect(res.body.users).to.be.an("array")
      expect(res.body.paginationInfo).to.be.an("object")
    })

    after("All tests, drop restore sandbox", async () => {
      await sandbox.restore()
    })
  })

  describe("/Post users, create new user", () => {
    before("All tests, mock create response", async () => {
      sandbox.stub(UserStore, "create").callsFake(async () => users[0])
    })

    it("should return status 200  and new user for success case", async () => {
      const request = await chai.request(server)
      const res = await request.post(`/v1/users`).send({ name: "Kodjo Desire Afewou" })
      res.status.should.eql(200)
      expect(res.body.user).to.be.an("object")
      assert(res.body.user.name === "Kodjo Desire Afewou", "user name is not approved")
    })

    it("should return status 422 for validation error", async () => {
      const request = await chai.request(server)
      const res = await request.post(`/v1/users`)
      res.status.should.eql(422)
      should().exist(res.error)
      assert(res.body.type === "validation_error", "error type not correct")
    })

    after("All tests, drop restore sandbox", async () => {
      await sandbox.restore()
    })
  })

  describe("/Post users/:id, update user", () => {
    before("All tests, mock methods", async () => {
      sandbox.stub(UserStore, "fetchByID").callsFake(async () => users[0])
      sandbox.stub(UserStore, "update").callsFake(async () => new UserObject({
        _id:  "5e358c634a0bfa40a0e654a0",
        name: "Kodjo Desire",
      } as IUser))
    })

    it("should return status 200 with updated user for successful user update", async () => {
      const request = await chai.request(server)
      const res = await request.post(`/v1/users/${users[0].id}`).send({ name: "Kodjo Desire" })
      res.status.should.eql(200)
      expect(res.body.user).to.be.an("object")
      assert(res.body.user.name === "Kodjo Desire", "user not is not correct")
    })

    it("should return status 422 for validation error", async () => {
      const request = await chai.request(server)
      const res = await request.post(`/v1/users/${users[0].id}`)
      res.status.should.eql(422)
      should().exist(res.error)
      assert(res.body.type === "validation_error", "error type not correct")
    })

    after("All tests, drop restore sandbox", async () => {
      await sandbox.restore()
    })
  })

  describe("/Post users/:id, update user", () => {
    before("All tests, mock fetchByID", async () => {
      sandbox.stub(UserStore, "fetchByID").callsFake(async () => null)
    })

    it("should return status 404 for and error", async () => {
      const request = await chai.request(server)
      const res = await request.post(`/v1/users/5e358c634a0bfa40a0`).send({ name: "Kodjo Desire" })
      res.status.should.eql(404)
      should().exist(res.error)
      assert(res.body.type === "resource_not_found", "error type not correct")
    })

    after("All tests, drop restore sandbox", async () => {
      await sandbox.restore()
    })
  })

  after("All tests, close server and drop restore sandbox", async () => {
    await sandbox.restore()
    await server.close()
  })
})
