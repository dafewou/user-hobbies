export default [
  {
    id:           "5e36a03c62f28b5e2b82a0da",
    passionLevel: "Medium",
    name:         "Coding",
    year:         "2010-12-09T00:00:00.000Z",
  },
  {
    id:           "5e36a25862f28b5e2b82a0db",
    passionLevel: "High",
    name:         "Playing football",
    year:         "2010-12-09T00:00:00.000Z",
  },
  {
    id:           "5e36a26e62f28b5e2b82a0dc",
    passionLevel: "High",
    name:         "Going to concert",
    year:         "2010-12-09T00:00:00.000Z",
  },
]
