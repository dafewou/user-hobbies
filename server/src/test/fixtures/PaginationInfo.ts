import PaginationInfo from "../../model/PaginationInfo"

export default new PaginationInfo(3, 1, 30)
