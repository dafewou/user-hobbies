import { IUser, UserObject } from "../../model/User"

export default [
  new UserObject({
    _id:  "5e358c634a0bfa40a0e654a0",
    name: "Kodjo Desire Afewou",
  } as IUser),
  new UserObject({
    _id:  "5e35c7b1511e6c4950f5b195",
    name: "Kodjo Desire AF",
  } as IUser),
  new UserObject({
    _id:  "5e369fd662f28b5e2b82a0d7",
    name: "Kodjo Desire",
  } as IUser),
]
