import mongoose, { Schema, Document } from "mongoose"

export enum passionLevel {
  Low = "Low",
  Medium = "Medium",
  High = "High",
  VeryHigh = "Very-High"
}

// Hobby document Interface
export interface IHobby extends Document {
  _id: string
  passionLevel: passionLevel
  name: string,
  year: string
}

// Hobby object
export class HobbyObject {
  id: string
  passionLevel: string
  name: string
  year: string

  constructor({ _id, passionLevel, name, year }: IHobby) {
    this.id = _id
    this.passionLevel = passionLevel
    this.name = name
    this.year = year
  }
}

// Create the hobby Schema.
const HobbySchema: Schema = new Schema({
  passionLevel: {
    type:     String,
    enum:     [passionLevel.Low, passionLevel.Medium, passionLevel.High, passionLevel.VeryHigh],
    required: true,
  },
  name:         {
    type:     String,
    required: true,
  },
  year:         {
    type:     Date,
    required: true,
  },
  user:         {
    type: Schema.Types.ObjectId,
    ref:  "User",
  },
})

HobbySchema.index({ name: 1, user: 1 }, { unique: true });

export default mongoose.model<IHobby>("Hobby", HobbySchema)
