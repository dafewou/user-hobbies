import mongoose, { Mongoose } from "mongoose"
import Config from "../helpers/Config"

const DATABASE_URL = Config.shared.requireProduction("DATABASE_URL", "mongodb://localhost:27017/user_hobbies")
let instance: Mongoose

export default class DB {
  static async shared(): Promise<Mongoose> {
    //check if db instance is not yet created
    if (instance == null) {
      // create singleton instance
      try {
        instance = await mongoose.connect(DATABASE_URL, {
          useCreateIndex:     true,
          useNewUrlParser:    true,
          useUnifiedTopology: true,
          useFindAndModify:   false,
        })

        return instance
      } catch (err) {
        console.log(`Could not connect to mongo DB: ${DATABASE_URL}`, err)
        process.exit()
      }
    }

    return instance
  }
}