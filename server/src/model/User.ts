import mongoose, { Schema, Document } from "mongoose"
import Hobby from "./Hobby"

// User document interface
export interface IUser extends Document {
  _id: string
  name: string,
}

// User object
export class UserObject {
  id: string
  name: string

  constructor({ _id, name }: IUser) {
    this.id = _id
    this.name = name
  }
}

// Create the user Schema.
const UserSchema: Schema = new Schema({
  name: {
    type:     String,
    required: true,
    unique:   true,
  },
})

// Middleware
UserSchema.pre("remove", async function (next) {
  // Remove user hobbies on user deletion
  await Hobby.remove({ user: this._id }).exec()
  next()
})

export default mongoose.model<IUser>("User", UserSchema)
