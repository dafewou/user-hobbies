# User-Hobbies

Application that list users and their hobbies.

## Setting up the development environment

I am using docker to setup the development environment.

### Setting up docker on Mac

```
# Installing docker
brew install docker docker-compose docker-machine
# Creating a docker machine called dev
docker-machine create dev --virtualbox-memory 8096 --virtualbox-disk-size 100000
```

### Building / Resetting docker services

```
# Removing any unused volume, images, containers
docker system prune -f

# Cloning the repository
cd /path/to/projects
git clone https://gitlab.com/dafewou/user-hobbies.git
cd ./user-hobbies

# Building docker images, this might take a while for the first time
docker-compose build
```

### Running docker

```
# Run the docker services
docker-compose up
```

### Useful commands

```
# For running unit testing
docker-compose run server npm run test

Now Point browser to http://localhost:8080/api-docs/ for swagger ui

Api available at `http://localhost:8080/v1`.
```

### Running each service

```
# Install mongodb on local machine and provide connection path in .env file:
 - [mongodb://localhost:27017/user_hobbies for example]

# Navigate to server folder and install dependencies
cd server/
npm i

# Run test
- npm run test

# Start server
- npm start
Server is listen on port 8080.

Swagger ui available at http://localhost:8080/api-docs/

Endpoints available at http://localhost:8080/v1
```
